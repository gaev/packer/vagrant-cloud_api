#!/bin/bash

# Login / password
LOGIN=GAEV
PASSWORD=

# Token
TOKEN=0

# Name of the box, the version and the provider
BOXNAME=
VERSION=
PROVIDER=virtualbox

# Path of the box file
VMFILE=

# (Re)start from STEP
STEP=0

function usage()
{
    echo "    -h | --help : display this screen"
    echo "    -l | --login=< a valid vagrant cloud login >"
    echo "    -p | --password=< the password corresponding to the login >"
    echo "    -t | --token=< a valid token returned at the step 1 >"
    echo "    -b | --boxname=< a box name >"
    echo "    -v | --version=< a version for the box name >"
    echo "    -i | --provider=< a valid provider (virtualbox(default)|vmware|...) >"
    echo "    -f | --vmpath=< the box file corresponding to the box name >"
    echo "    -s | --step=< the step at which the data are (re)submitted  >"
    echo ""
    echo "Steps: "
    echo "=== Step 1: Create a Token"
    echo "=== Step 2: Create a Box"
    echo "=== Step 3: Create a Version"
    echo "=== Step 4: Create a Provider"
    echo "=== Step 5: Upload the box file"
    echo "=== Step 6: Release the Version"
}

function view_user_params()
{
    echo "    -l | --login=$LOGIN"
    echo "    -p | --password=$PASSWORD"
    echo "    -t | --token=$TOKEN"
    echo "    -b | --boxname=$BOXNAME"
    echo "    -v | --version=$VERSION"
    echo "    -i | --provider=$PROVIDER"
    echo "    -f | --vmpath=$VMFILE"
    echo "    -s | --step=$STEP"
    echo ""
}

HELP=0
while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            HELP=1
            ;;
        -l | --login)
            LOGIN=$VALUE
            ;;
        -p | --password)
            PASSWORD=$VALUE
            ;;
        -t | --token)
            TOKEN=$VALUE
            ;;
        -b | --boxname)
            BOXNAME=$VALUE
            ;;
        -v | --version)
            VERSION=$VALUE
            ;;
        -i | --provider)
            PROVIDER=$VALUE
            ;;
        -f | --vmpath)
            VMFILE=$VALUE
            ;;
        -s | --step)
            STEP=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

if [ $HELP -eq 0 ]; then
  echo
  view_user_params
  echo
  
  unset VAGRANT_RET
  unset VAGRANT_DATA
  unset VAGRANT_JSON
  
  SH=.
  SCRIPTS=./scripts
  
  # $SH $SCRIPTS/reset.sh
  
  # Export all VAGRANT variables based on the variable $STEP
  # This means that all steps up to and including the $STEP step are successfully completed.
  # The script will start from $STEP + 1 
  [ $STEP -gt 0 ] && export VAGRANT_TOKEN=$TOKEN         # step 1
  [ $STEP -gt 1 ] && export VAGRANT_BOXNAME=$BOXNAME     # step 2
  [ $STEP -gt 2 ] && export VAGRANT_VERSION=$VERSION     # step 3
  [ $STEP -gt 3 ] && export VAGRANT_PROVIDER=$PROVIDER   # step 4
  [ $STEP -gt 4 ] && export VAGRANT_UPLOAD=1             # step 5
  
  export VAGRANT_RET=0
  export VAGRANT_STEP=0
  export VAGRANT_LOGIN=$LOGIN
  export VAGRANT_VMFILE=$VMFILE
  
  
  echo "=== Step 1: Create a Token ====================================="
  [[ ("$VAGRANT_TOKEN" == "" || "$VAGRANT_TOKEN" == "0") && $VAGRANT_RET -eq 0 ]] && $SH $SCRIPTS/create_token.sh $LOGIN $PASSWORD
  [ $VAGRANT_RET -eq 0 ] && export VAGRANT_STEP=$(expr $VAGRANT_STEP + 1)
  echo
  
  echo "=== Step 2: Create a Box ======================================="
  [[ ("$VAGRANT_BOXNAME" == "" || "$VAGRANT_BOXNAME" == "0") && $VAGRANT_RET -eq 0 ]] && $SH $SCRIPTS/create_box.sh $BOXNAME
  [ $VAGRANT_RET -eq 0 ] && export VAGRANT_STEP=$(expr $VAGRANT_STEP + 1)
  echo
  
  echo "=== Step 3: Create a Version ==================================="
  [[ ("$VAGRANT_VERSION" == "" || "$VAGRANT_VERSION" == "0") && $VAGRANT_RET -eq 0 ]] && $SH $SCRIPTS/create_version.sh $VERSION
  [ $VAGRANT_RET -eq 0 ] && export VAGRANT_STEP=$(expr $VAGRANT_STEP + 1)
  echo
  
  echo "=== Step 4: Create a Provider =================================="
  [[ ("$VAGRANT_PROVIDER" == "" || "$VAGRANT_PROVIDER" == "0") && $VAGRANT_RET -eq 0 ]] && $SH $SCRIPTS/create_provider.sh $PROVIDER
  [ $VAGRANT_RET -eq 0 ] && export VAGRANT_STEP=$(expr $VAGRANT_STEP + 1)
  echo
  
  echo "=== Step 5: Upload the box file ================================"
  [[ ("$VAGRANT_UPLOAD" == "" || "$VAGRANT_UPLOAD" == "0") && $VAGRANT_RET -eq 0 ]] && $SH $SCRIPTS/upload_provider.sh $VMFILE
  [ $VAGRANT_RET -eq 0 ] && export VAGRANT_STEP=$(expr $VAGRANT_STEP + 1)
  echo
  
  echo "=== Step 6: Release the Version ================================"
  [ $VAGRANT_STEP -eq 5 ] && $SH $SCRIPTS/release_version.sh
fi
