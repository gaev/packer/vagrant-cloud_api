#!/bin/bash

unset VAGRANT_TOKEN
unset VAGRANT_DATA
unset VAGRANT_JSON

export VAGRANT_RET=1

if [ $# -lt 2 ]; then
   echo "Usage: . $0 login password"
else
   VAGRANT_LOGIN=$1
   PASSWORD=$2
   export VAGRANT_LOGIN

   VAGRANT_DATA='{"token":{"description":"Login from cURL"},"user":{"login":"'${VAGRANT_LOGIN}'","password":"'${PASSWORD}'"}}'
   export VAGRANT_DATA
   VAGRANT_JSON=$( curl --header "Content-Type: application/json" https://app.vagrantup.com/api/v1/authenticate  --data "$VAGRANT_DATA" )
   export VAGRANT_JSON

   RET=$(echo $VAGRANT_JSON | grep errors 1>/dev/null 2>/dev/null; echo $?)
   if [ $RET -eq 1 ]; then
      export VAGRANT_TOKEN=$(echo $VAGRANT_JSON | cut -d',' -f2 | cut -d':' -f2 | sed -e "s/\"//g")
      export VAGRANT_RET=0
   else
      export VAGRANT_TOKEN=0
   fi

   env | grep VAGRANT
fi

#curl --header "Authorization: Bearer $VAGRANT_TOKEN" https://app.vagrantup.com/api/v1/user/$VAGRANT_LOGIN
#curl --header "Authorization: Bearer $VAGRANT_TOKEN" --request DELETE https://app.vagrantup.com/api/v1/authenticate