#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Usage: . ./get_box_info.sh boxname [provider]"
else
   BOXNAME=$1
   PROVIDER=virtualbox
   [ $# -gt 1 ] && PROVIDER=$2

  response=$(curl --header "Authorization: Bearer ${VAGRANT_TOKEN}" \
               https://app.vagrantup.com/api/v1/search?q=${BOXNAME}&&provider=${PROVIDER})

  RET=$(echo $response | grep "404 Not Found" 1>/dev/null 2>/dev/null; echo $?)

  [ $RET -ne 0 ] && echo $response
  [ $RET -eq 0 ] && echo "404 Not Found"
fi




