#!/bin/bash

unset VAGRANT_VERSION
unset VAGRANT_DATA
unset VAGRANT_JSON

export VAGRANT_RET=1

if [ $# -lt 1 ]; then
   echo "Usage: . $0 version"
else
   VAGRANT_VERSION=$1
   export VAGRANT_VERSION

   VAGRANT_DATA='{"version":{"version":"'${VAGRANT_VERSION}'","description":""}}'
   export VAGRANT_DATA

   VAGRANT_JSON=$(curl --header "Content-Type: application/json" \
               --header "Authorization: Bearer $VAGRANT_TOKEN" https://app.vagrantup.com/api/v1/box/$VAGRANT_LOGIN/$VAGRANT_BOXNAME/versions \
               --data "$VAGRANT_DATA")   
   export VAGRANT_JSON
   
   RET=$(echo $VAGRANT_JSON | grep errors 1>/dev/null 2>/dev/null; echo $?)
   [ $RET -eq 0 ] && export VAGRANT_VERSION=0
   [ $RET -ne 0 ] && export VAGRANT_RET=0

   env | grep VAGRANT
fi


