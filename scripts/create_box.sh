#!/bin/bash

unset VAGRANT_BOXNAME
unset VAGRANT_DATA
unset VAGRANT_JSON

export VAGRANT_RET=1

if [ $# -lt 1 ]; then
   echo "Usage: . $0 boxname"
else
   VAGRANT_BOXNAME=$1
   export VAGRANT_BOXNAME

   VAGRANT_DATA='{"box":{"username":"'${VAGRANT_LOGIN}'","name":"'${VAGRANT_BOXNAME}'","short_description":"","description":"","is_private":false}}'
   export VAGRANT_DATA

   VAGRANT_JSON=$(curl --header "Content-Type: application/json" \
               --header "Authorization: Bearer $VAGRANT_TOKEN" https://app.vagrantup.com/api/v1/boxes \
               --data "$VAGRANT_DATA")   
   export VAGRANT_JSON
   
   RET=$(echo $VAGRANT_JSON | grep errors 1>/dev/null 2>/dev/null; echo $?)
   [ $RET -eq 0 ] && export VAGRANT_BOXNAME=0
   [ $RET -ne 0 ] && export VAGRANT_RET=0

   env | grep VAGRANT
fi


