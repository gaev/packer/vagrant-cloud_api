#!/bin/bash

unset VAGRANT_PROVIDER
unset VAGRANT_DATA
unset VAGRANT_JSON

export VAGRANT_RET=1

if [ $# -lt 1 ]; then
   echo "Usage: . $0 provider"
else
   VAGRANT_PROVIDER=$1
   export VAGRANT_PROVIDER

   checksum=""
   [[ ("$VAGRANT_VMFILE" != "" && "$VAGRANT_VMFILE" != "0") ]] && checksum=$(md5sum $VAGRANT_VMFILE | cut -d' ' -f1)

   [ "$checksum" == "" ] &&  VAGRANT_DATA='{"provider":{"name":"'${VAGRANT_PROVIDER}'"}}'
   [ "$checksum" != "" ] &&  VAGRANT_DATA='{"provider":{"name":"'${VAGRANT_PROVIDER}'","checksum":"'${checksum}'", "checksum_type":"md5"}}'
   export VAGRANT_DATA

   VAGRANT_JSON=$(curl --header "Content-Type: application/json" \
               --header "Authorization: Bearer $VAGRANT_TOKEN" \
               https://app.vagrantup.com/api/v1/box/$VAGRANT_LOGIN/$VAGRANT_BOXNAME/version/$VAGRANT_VERSION/providers \
               --data "$VAGRANT_DATA")
   export VAGRANT_JSON
   
   RET=$(echo $VAGRANT_JSON | grep errors 1>/dev/null 2>/dev/null; echo $?)
   [ $RET -eq 0 ] && export VAGRANT_PROVIDER=0
   [ $RET -ne 0 ] && export VAGRANT_RET=0

   env | grep VAGRANT
fi


