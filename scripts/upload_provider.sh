#!/bin/bash

unset VAGRANT_UPLOAD
unset VAGRANT_DATA
unset VAGRANT_JSON

export VAGRANT_RET=1

if [ $# -lt 1 ]; then
   echo "Usage: . $0 <path of the box>"
else
   VAGRANT_VMFILE=$1
   export VAGRANT_VMFILE

   VAGRANT_DATA=$(curl --header "Authorization: Bearer $VAGRANT_TOKEN" \
               https://app.vagrantup.com/api/v1/box/$VAGRANT_LOGIN/$VAGRANT_BOXNAME/version/$VAGRANT_VERSION/provider/$VAGRANT_PROVIDER/upload )
   export VAGRANT_DATA

   upload_path=$(echo "$VAGRANT_DATA" | cut -d'"' -f4 )
   echo
   echo curl --request PUT --upload-file "$VAGRANT_VMFILE" "$upload_path"
   curl --request PUT --upload-file "$VAGRANT_VMFILE" "$upload_path" && RET=$?
   echo

   [ $RET -ne 0 ] && export VAGRANT_UPLOAD=0
   [ $RET -eq 0 ] && export VAGRANT_UPLOAD=1 && export VAGRANT_RET=0

   env | grep VAGRANT
fi


