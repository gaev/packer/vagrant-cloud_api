# vagrant-cloud_API

Tool to submit a Vagrant box using the Vagrant Cloud API.
See https://www.vagrantup.com/vagrant-cloud/api.html for details on the API.

**Registering a "box" consists of 5 steps**
* Creation of an entry for the "box": you must specify at least the name of the box (boxname). Its description is optional.
* Creating a version: there can be several versions of the same entry; the version must be specified.
* Creation of a provider: likewise, for an entry (boxname) and a version, there can be several associated providers; the provider must be specified.
* Download the box file.
* Validate the box. (entry + version + provider)

All these steps can be done either through the Vagrant Cloud web interface or through multiple API calls. Indeed, each step corresponds to a specific API call (two calls in the case of download). In addition, each API call requires authentication using a token. A token is a time-limited key. It is therefore necessary to first obtain a token by using a specific API call and specifying its login and password. It is therefore necessary to have previously created an account on Vagrant Cloud. Once the token is obtained, it will be used as an authentication mechanism for the other API calls.

In order to facilitate the automation of recordings, an application written in Bash Shell has been developed.  

**Usage of the shell script**

```
$ sh ./register_box_in_cloud.sh -h
    -h | --help : display this screen
    -l | --login=< a valid vagrant cloud login >
    -p | --password=< the password corresponding to the login >
    -t | --token=< a valid token returned at the step 1 >
    -b | --boxname=< a box name >
    -v | --version=< a version for the box name >
    -i | --provider=< a valid provider (virtualbox(default)|vmware|...) >
    -f | --vmpath=< the box file corresponding to the box name >
    -s | --step=< the step at which the data are (re)submitted  >

Steps:
=== Step 1: Create a Token
=== Step 2: Create a Box
=== Step 3: Create a Version
=== Step 4: Create a Provider
=== Step 5: Upload the box file
=== Step 6: Release the Version
```

The use of this script can be done in two ways:
* by performing all the steps in a single call: so you have to specify all the necessary parameters. In this case, you can execute the script by invoking a new shell session (i.e. invocation by 'sh'), or execute it in the same shell session, and in the latter case, a restart from a step (in case of failure for example) can be done.
* by executing step by step, specifying the step number and specifying the parameters needed for the step. In this case, the script must be executed in the current shell session (i.e. invocation by '.').


**Example**

```
$ sh ./register_box_in_cloud.sh -l=GAEV -p=XXXXXXX -b=centos7-dd8Gb -v=1.1.0 -f=../boxes/virtualbox-centos7_8Gb.box
```

```
    -l | --login=GAEV
    -p | --password=XXXXXXX
    -t | --token=0
    -b | --boxname=centos7-dd8Gb
    -v | --version=1.1.0
    -i | --provider=virtualbox
    -f | --vmpath=../boxes/virtualbox-centos7_8Gb.box
    -s | --step=0


=== Step 1: Create a Token =====================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   437    0   345  100    92    436    116 --:--:-- --:--:-- --:--:--   553
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_JSON={"description":"Login from cURL","token":"UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao","token_hash":"96ff1be6879d772d309d541950b9d266cb7ff45d2c72f3c81951f8c7ca5cb77b4047859ba00a806f4d7ece6ba9218806a4cebf69f0f64a6fd148cc621460f51f","created_at":"2020-09-24T09:14:53.377Z","user":{"username":"GAEV"}}
VAGRANT_STEP=0
VAGRANT_RET=0
VAGRANT_DATA={"token":{"description":"Login from cURL"},"user":{"login":"GAEV","password":"XXXXXXX"}}

=== Step 2: Create a Box =======================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   408    0   299  100   109    401    146 --:--:-- --:--:-- --:--:--   549
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_BOXNAME=centos7-dd8Gb
VAGRANT_JSON={"tag":"GAEV/centos7-dd8Gb","username":"GAEV","name":"centos7-dd8Gb","private":false,"downloads":0,"created_at":"2020-09-24T11:14:54.493+02:00","updated_at":"2020-09-24T11:14:54.493+02:00","short_description":"","description_markdown":"","description_html":null,"current_version":null,"versions":[]}
VAGRANT_STEP=1
VAGRANT_RET=0
VAGRANT_DATA={"box":{"username":"GAEV","name":"centos7-dd8Gb","short_description":"","description":"","is_private":false}}

=== Step 3: Create a Version ===================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   447    0   399  100    48    604     72 --:--:-- --:--:-- --:--:--   677
VAGRANT_VERSION=1.1.0
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_BOXNAME=centos7-dd8Gb
VAGRANT_JSON={"version":"1.1.0","status":"unreleased","description_html":null,"description_markdown":"","created_at":"2020-09-24T11:14:55.396+02:00","updated_at":"2020-09-24T11:14:55.396+02:00","number":"1.1.0","release_url":"https://app.vagrantup.com/api/v1/box/GAEV/centos7-dd8Gb/version/1.1.0/release","revoke_url":"https://app.vagrantup.com/api/v1/box/GAEV/centos7-dd8Gb/version/1.1.0/revoke","providers":[]}
VAGRANT_STEP=2
VAGRANT_RET=0
VAGRANT_DATA={"version":{"version":"1.1.0","description":""}}

=== Step 4: Create a Provider ==================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   443    0   340  100   103    469    142 --:--:-- --:--:-- --:--:--   611
VAGRANT_VERSION=1.1.0
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_PROVIDER=virtualbox
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_BOXNAME=centos7-dd8Gb
VAGRANT_JSON={"name":"virtualbox","hosted":true,"hosted_token":null,"original_url":null,"created_at":"2020-09-24T11:14:58.192+02:00","updated_at":"2020-09-24T11:14:58.192+02:00","download_url":"https://vagrantcloud.com/GAEV/boxes/centos7-dd8Gb/versions/1.1.0/providers/virtualbox.box","checksum":"1e2d6d5aef2c844d93b6e8ddabe2c203","checksum_type":"md5"}
VAGRANT_STEP=3
VAGRANT_RET=0
VAGRANT_DATA={"provider":{"name":"virtualbox","checksum":"1e2d6d5aef2c844d93b6e8ddabe2c203", "checksum_type":"md5"}}

=== Step 5: Upload the box file ================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   347    0   347    0     0    483      0 --:--:-- --:--:-- --:--:--   482

curl --request PUT --upload-file ../boxes/virtualbox-centos7_8Gb.box https://archivist.vagrantup.com/v1/object/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJib3hlcy8yNTkwMTgyMS1kYWE0LTRiYjgtYjA0ZC03YzIyMDIzZmZiYzUiLCJtb2RlIjoidyIsImV4cGlyZSI6MTYwMDkzOTc5OSwiY2FsbGJhY2siOiJodHRwczovL2FwcC52YWdyYW50dXAuY29tL2FwaS9pbnRlcm5hbC9hcmNoaXZpc3QvY2FsbGJhY2sifQ.6lQviyoIosEjuwpYf7cRSQ9jAaZxlZd5SnHiI5FN4AM
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  922M    0     0  100  922M      0  11.0M  0:01:23  0:01:23 --:--:-- 9447k

VAGRANT_VERSION=1.1.0
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_PROVIDER=virtualbox
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_BOXNAME=centos7-dd8Gb
VAGRANT_STEP=4
VAGRANT_UPLOAD=1
VAGRANT_RET=0
VAGRANT_DATA={"upload_path":"https://archivist.vagrantup.com/v1/object/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJib3hlcy8yNTkwMTgyMS1kYWE0LTRiYjgtYjA0ZC03YzIyMDIzZmZiYzUiLCJtb2RlIjoidyIsImV4cGlyZSI6MTYwMDkzOTc5OSwiY2FsbGJhY2siOiJodHRwczovL2FwcC52YWdyYW50dXAuY29tL2FwaS9pbnRlcm5hbC9hcmNoaXZpc3QvY2FsbGJhY2sifQ.6lQviyoIosEjuwpYf7cRSQ9jAaZxlZd5SnHiI5FN4AM"}

=== Step 6: Release the Version ================================
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   735    0   735    0     0      4      0 --:--:--  0:02:30 --:--:--   161
VAGRANT_VERSION=1.1.0
VAGRANT_TOKEN=UpeX2akqehYL3g.atlasv1.hcqosQXxJ8V9ILmWXeTUJyfWbD8BB1NuLgyEkMhqqGhFoTVkftJAmq8Htjw2hyH3Vao
VAGRANT_PROVIDER=virtualbox
VAGRANT_LOGIN=GAEV
VAGRANT_VMFILE=../boxes/virtualbox-centos7_8Gb.box
VAGRANT_BOXNAME=centos7-dd8Gb
VAGRANT_JSON={"version":"1.1.0","status":"active","description_html":null,"description_markdown":"","created_at":"2020-09-24T11:14:55.396+02:00","updated_at":"2020-09-24T11:18:54.029+02:00","number":"1.1.0","release_url":"https://app.vagrantup.com/api/v1/box/GAEV/centos7-dd8Gb/version/1.1.0/release","revoke_url":"https://app.vagrantup.com/api/v1/box/GAEV/centos7-dd8Gb/version/1.1.0/revoke","providers":[{"name":"virtualbox","hosted":true,"hosted_token":null,"original_url":null,"created_at":"2020-09-24T11:14:58.192+02:00","updated_at":"2020-09-24T11:14:58.192+02:00","download_url":"https://vagrantcloud.com/GAEV/boxes/centos7-dd8Gb/versions/1.1.0/providers/virtualbox.box","checksum":"1e2d6d5aef2c844d93b6e8ddabe2c203","checksum_type":"md5"}]}
VAGRANT_STEP=5
VAGRANT_UPLOAD=1
VAGRANT_RET=0
VAGRANT_DATA={"upload_path":"https://archivist.vagrantup.com/v1/object/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJib3hlcy8yNTkwMTgyMS1kYWE0LTRiYjgtYjA0ZC03YzIyMDIzZmZiYzUiLCJtb2RlIjoidyIsImV4cGlyZSI6MTYwMDkzOTc5OSwiY2FsbGJhY2siOiJodHRwczovL2FwcC52YWdyYW50dXAuY29tL2FwaS9pbnRlcm5hbC9hcmNoaXZpc3QvY2FsbGJhY2sifQ.6lQviyoIosEjuwpYf7cRSQ9jAaZxlZd5SnHiI5FN4AM"}
```

